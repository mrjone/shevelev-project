/***************************************************************
 * Name:      shevelevjobApp.h
 * Purpose:   Defines Application Class
 * Author:    Dmitry Arkeniev (cool.iridiy@mail.ru)
 * Created:   2019-03-23
 * Copyright: Dmitry Arkeniev ()
 * License:
 **************************************************************/

#ifndef SHEVELEVJOBAPP_H
#define SHEVELEVJOBAPP_H

#include <wx/app.h>

class shevelevjobApp : public wxApp
{
    public:
        virtual bool OnInit();
};

#endif // SHEVELEVJOBAPP_H
