/***************************************************************
 * Name:      shevelevjobApp.cpp
 * Purpose:   Code for Application Class
 * Author:    Dmitry Arkeniev (cool.iridiy@mail.ru)
 * Created:   2019-03-23
 * Copyright: Dmitry Arkeniev ()
 * License:
 **************************************************************/

#include "shevelevjobApp.h"

//(*AppHeaders
#include "shevelevjobMain.h"
#include <wx/image.h>
//*)

IMPLEMENT_APP(shevelevjobApp);

bool shevelevjobApp::OnInit()
{
    //(*AppInitialize
    bool wxsOK = true;
    wxInitAllImageHandlers();
    if ( wxsOK )
    {
    	shevelevjobFrame* Frame = new shevelevjobFrame(0);
    	Frame->Show();
    	SetTopWindow(Frame);
    }
    //*)
    return wxsOK;

}
