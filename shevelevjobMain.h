/***************************************************************
 * Name:      shevelevjobMain.h
 * Purpose:   Defines Application Frame
 * Author:    Dmitry Arkeniev (cool.iridiy@mail.ru)
 * Created:   2019-03-23
 * Copyright: Dmitry Arkeniev ()
 * License:
 **************************************************************/

#ifndef SHEVELEVJOBMAIN_H
#define SHEVELEVJOBMAIN_H

//(*Headers(shevelevjobFrame)
#include <wx/frame.h>
#include <wx/menu.h>
#include <wx/statusbr.h>
//*)

class shevelevjobFrame: public wxFrame
{
    public:

        shevelevjobFrame(wxWindow* parent,wxWindowID id = -1);
        virtual ~shevelevjobFrame();

    private:

        //(*Handlers(shevelevjobFrame)
        void OnQuit(wxCommandEvent& event);
        void OnAbout(wxCommandEvent& event);
        //*)

        //(*Identifiers(shevelevjobFrame)
        static const long idMenuQuit;
        static const long idMenuAbout;
        static const long ID_STATUSBAR1;
        //*)

        //(*Declarations(shevelevjobFrame)
        wxStatusBar* StatusBar1;
        //*)

        DECLARE_EVENT_TABLE()
};

#endif // SHEVELEVJOBMAIN_H
